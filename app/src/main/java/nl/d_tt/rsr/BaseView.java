package nl.d_tt.rsr;

public interface BaseView<T> {
    void setPresenter(T presenter);
}
