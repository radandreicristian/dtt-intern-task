package nl.d_tt.rsr.ui.map;

import android.location.Location;

import nl.d_tt.rsr.BasePresenter;
import nl.d_tt.rsr.BaseView;

public interface MapContract {

    @SuppressWarnings("unused")
    interface MapView extends BaseView<MapPresenter> {

        void generateMap();

        void updateLocation(Location Location);

        void updateMarker(Location location);

    }

    @SuppressWarnings("unused")
    interface MapPresenter extends BasePresenter {

        void updateLocationOnMap(Location location);
    }
}