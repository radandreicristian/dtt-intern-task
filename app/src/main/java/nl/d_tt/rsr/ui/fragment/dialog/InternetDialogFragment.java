package nl.d_tt.rsr.ui.fragment.dialog;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import java.util.Objects;

import nl.d_tt.rsr.R;

public class InternetDialogFragment extends DialogFragment {

    //Empty constructor, will call onCreateDialog automatically
    public InternetDialogFragment() {

    }

    public static InternetDialogFragment newInstance() {
        return new InternetDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()), R.style.AlertDialogTheme);

        alertDialogBuilder.setTitle(getResources().getString(R.string.internet_dialog_title))
                .setMessage(getResources().getString(R.string.internet_dialog_msg))
                .setNegativeButton(getString(R.string.map_dialog_positive_btn_msg), (dialog, which) ->
                        getActivity().finish()
                )
                .setPositiveButton(getString(R.string.map_dialog_negative_btn_msg), (dialog, which) -> {
                    Intent myIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                    startActivity(myIntent);
                });
        setCancelable(false);

        return alertDialogBuilder.create();
    }
}
