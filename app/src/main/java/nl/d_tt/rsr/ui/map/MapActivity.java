package nl.d_tt.rsr.ui.map;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

import nl.d_tt.rsr.R;
import nl.d_tt.rsr.ui.fragment.dialog.CallDialogFragment;
import nl.d_tt.rsr.ui.fragment.dialog.InternetDialogFragment;
import nl.d_tt.rsr.ui.fragment.dialog.LocationDialogFragment;


public class MapActivity extends AppCompatActivity implements OnMapReadyCallback, MapContract.MapView {

    public static final String TAG =  MapActivity.class.getSimpleName();

    private MapContract.MapPresenter mPresenter;

    //Fields for google maps
    private GoogleMap mMap;
    private Marker mMarker;

    private static final int LOCATION_ZOOM = 15;

    //Constants for permissions
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    private static final int PERMISSIONS_REQUEST_ACCESS_CALL = 2;

    //Fields for permissions (false, by default)
    private boolean mHasLocationPermissionGranted;
    private boolean mHasCallPermissionGranted;

    private FragmentManager mFragmentManager;

    //Fields for dialog fragments
    private LocationDialogFragment mLocationDialogFragment;
    private InternetDialogFragment mInternetDialogFragment;
    private CallDialogFragment mCallDialogFragment;

    private CustomInfoWindowAdapter mCustomInfoWindowAdapter;

    private boolean mIsLocationEnabled;
    private boolean mIsInternetEnabled;
    private boolean mIsUpdatable;

    private LocationManager mLocationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        mPresenter = new MapPresenter(this);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //initially true so additional calls are not performed in the very first onResume (the one that gets called after onStart)
        mIsLocationEnabled = true;
        mIsInternetEnabled = true;
        mIsUpdatable = false;

        mFragmentManager = getSupportFragmentManager();
        mLocationDialogFragment = LocationDialogFragment.newInstance();
        mInternetDialogFragment = InternetDialogFragment.newInstance();
        mCallDialogFragment = CallDialogFragment.newInstance();

        mCustomInfoWindowAdapter = new CustomInfoWindowAdapter(this);

        Button toolbarBackButton = findViewById(R.id.activity_map_toolbar_button);
        toolbarBackButton.setOnClickListener(v -> goToMainMenu());

        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        if (!isTablet) {
            Button callDialogButton = findViewById(R.id.activity_map_button);
            callDialogButton.setOnClickListener(v ->
                    showCallDialog()
            );
        }
        generateMap();
    }

    @Override
    public void onResume() {
        super.onResume();

        //Register the broadcastreceivers every time the application becomes visible to the user
        registerReceiver(locationBroadcastReceiver, new IntentFilter(LocationManager.PROVIDERS_CHANGED_ACTION));
        registerReceiver(internetBroadcastReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));

        /*
            The following logic and calls are implemented especially for the following scenario (which does not work as intended in the model version)
            The user has location enabled, disables it, puts the activity on pause, turns location on, and then returns to the application.
            The alert dialog should disappear if you resume with location enabled.
         */
        if (!mIsLocationEnabled) {
            checkLocationEnabled();
        }
        if (!mIsInternetEnabled) {
            checkInternetConnectionEnabled();
        }
        if (mIsUpdatable) {
            try {
                updateLocation(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, (MapPresenter) mPresenter);
            } catch (SecurityException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        mLocationManager.removeUpdates((MapPresenter)mPresenter);
        /*
            Unregister the broadcastreceivers every time the application is no longer visible to the user
            becauase we only want them active when the activity is visible to the user, to avoid leaks
         */
        unregisterReceiver(locationBroadcastReceiver);
        unregisterReceiver(internetBroadcastReceiver);
    }

    @Override
    public void generateMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) mFragmentManager.findFragmentById(R.id.activity_map_map);
        if(mapFragment != null){
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void updateLocation(Location location) {
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(),
                        location.getLongitude()), mMap.getCameraPosition().zoom));
    }

    @Override
    public void updateMarker(Location location) {
        if (mMarker != null) {
            mMarker.remove();
        }
        mMarker = mMap.addMarker(new MarkerOptions()
                .icon(BitmapDescriptorFactory.fromBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.map_marker)))
                .position(new LatLng(location.getLatitude(), location.getLongitude()))
                .snippet(getCompleteAddress(location.getLatitude(), location.getLongitude())));
        mMarker.showInfoWindow();
    }

    @Override
    public void setPresenter(MapContract.MapPresenter presenter) {
        mPresenter = presenter;
    }

    /*
        Method for checking if the user has granted location permission
        If yes, we remember the decision in a boolean field so the user does not get asked
        again after he allowed it
     */
    private void getLocationPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mHasLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
    }

    /*
      Method for checking if the user has granted call permission
      If yes, we remember the decision in a boolean field so the user does not get asked
      again after he allowed it
   */
    private void getCallPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
            mHasCallPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMISSIONS_REQUEST_ACCESS_CALL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permission[], @NonNull int[] grantResults) {
        /*
            Chang the state of the boolean variables that tell us if we have permisisons based on what the requestpermission retruns
         */
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mHasLocationPermissionGranted = true;
                    enableLocation();

                }
                /*
                    If the permission is denied, the activity finishes, returning to the previous screen. If we forcefully disable the location permission,
                    the user will be unable to access the map activity (it will instantly close when it is opened)
                 */
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                    finish();
                }
                break;
            }
            case PERMISSIONS_REQUEST_ACCESS_CALL: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mHasCallPermissionGranted = true;
                    //perform the call method as soon as the permission is granted. we can pass null as the view since it won't affect the behaviour of the method
                    call(null);
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    public void onMapReady(GoogleMap map) {

        mMap = map;

        getLocationPermission();

        checkLocationEnabled();

        checkInternetConnectionEnabled();

        enableLocation();

        getDeviceLocation();

        /*
            The info window theme is set to the one defined in the CustomInfoWindowAdapter
         */
        mMap.setInfoWindowAdapter(mCustomInfoWindowAdapter);

        /*
            Remove the two buttons from the top of the map (my locaiton and compass)
         */
        mMap.getUiSettings().setMyLocationButtonEnabled(false);

        mMap.getUiSettings().setCompassEnabled(false);

        /*
        The map is updatable only after everything else in the callback has finished (map is initialized).
        This prevents the first onResume call (when the app is started) to try to update a null map resulting in a npe
        */
        mIsUpdatable = true;
    }

    private void enableLocation() {
        if (mMap == null) {
            return;
        }
        try {
            mMap.setMyLocationEnabled(false);
            if (!mHasLocationPermissionGranted) {
                getLocationPermission();
            } else {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude(),
                                mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude()), LOCATION_ZOOM));
                updateMarker(mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
            }
        } catch (SecurityException e) {
            Log.e(TAG, e.getMessage());
        }
    }

    private void getDeviceLocation() {
        try {
            if (mHasLocationPermissionGranted) {
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, (MapPresenter) mPresenter);
            } else {
                // Prompt the user for permission.
                getLocationPermission();
            }
        } catch (SecurityException e) {
            Log.e("Security:", e.getMessage());
        }

    }

    private String getCompleteAddress(double latitude, double longitude) {
         /*
     Compute the complete address by using geocoder, based on latitude and longitude
     */

        String completeAddress = "";
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
            if (addressList != null) {
                Address returnAddress = addressList.get(0);
                completeAddress = completeAddress + returnAddress.getThoroughfare() + " " + returnAddress.getSubThoroughfare() +
                        ", " + returnAddress.getLocality() + " " + returnAddress.getPostalCode() + ", " + returnAddress.getCountryName();
            }
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }

        return completeAddress;

    }

    /*

       Check if the user has location enabled. If the test fails, the location dialog fragment
       becomes visible to the user. If not (meaning that the conneciton is enabled), we dismiss the already
       existin
    */
    private void checkLocationEnabled() {

        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            if (mLocationDialogFragment.isAdded()) {
                return;
            }
            mLocationDialogFragment.show(mFragmentManager, "");
            mIsLocationEnabled = false;
        } else {
            if (mLocationDialogFragment.isAdded()) {
                mLocationDialogFragment.dismiss();
            }
            mIsLocationEnabled = true;
        }
    }

    /*
        Check if the user has internet connection. If the test fails, the internet dialog fragment
        becomes visible to the user. If not (meaning that the conneciton is enabled), we dismiss the already
        existing fragment, if it exists.
     */
    private void checkInternetConnectionEnabled() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() == null) {
            if (mInternetDialogFragment.isAdded()) {
                return;
            }
            mInternetDialogFragment.show(mFragmentManager, "");
            mIsInternetEnabled = false;
        } else {
            if (mInternetDialogFragment.isAdded()) {
                mInternetDialogFragment.dismiss();
            }
            mIsInternetEnabled = true;
        }
    }


    private final BroadcastReceiver locationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkLocationEnabled();
        }
    };

    private final BroadcastReceiver internetBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            checkInternetConnectionEnabled();
        }
    };


    //method called from calldialogfragment's xml's onClick, hence why passing a View
    @SuppressWarnings("unused")
    public void dismissCallDialog(View view) {
        if (mCallDialogFragment.isAdded()) {
            mCallDialogFragment.dismiss();
            Button callDialogButton = findViewById(R.id.activity_map_button);
            callDialogButton.setVisibility(View.VISIBLE);
        }
    }

    //method called from calldialogfragment's xml's onClick, hence why passing a View
    @SuppressWarnings("unused")
    public void call(View view) {
        if (mHasCallPermissionGranted) {
            Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + getResources().getString(R.string.msg_phone_number)));
            startActivity(intent);
        } else {
            getCallPermission();
        }
    }

    public void showCallDialog() {
        Button callDialogButton = findViewById(R.id.activity_map_button);
        mCallDialogFragment.show(mFragmentManager, "");
        callDialogButton.setVisibility(View.INVISIBLE);
    }

    public void goToMainMenu() {
        finish();
    }
}
