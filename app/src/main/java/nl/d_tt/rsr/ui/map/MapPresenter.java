package nl.d_tt.rsr.ui.map;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

public class MapPresenter implements LocationListener, MapContract.MapPresenter {

    private final MapContract.MapView mMapView;

    public MapPresenter(MapContract.MapView mapView) {
        mMapView = mapView;
        mapView.setPresenter(this);
    }

    @Override
    public void onLocationChanged(Location location){
        updateLocationOnMap(location);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void updateLocationOnMap(Location location){
        mMapView.updateMarker(location);
    }

}
