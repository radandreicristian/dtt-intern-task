package nl.d_tt.rsr.ui.fragment.dialog;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

import java.util.Objects;

import nl.d_tt.rsr.R;

public class InformationDialogFragment extends DialogFragment {


    //Empty constructor, will call onCreateDialog automatically
    public InformationDialogFragment() {

    }

    public static InformationDialogFragment newInstance() {
        return new InformationDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Objects.requireNonNull(getActivity()));

        alertDialogBuilder.setTitle(getResources().getString(R.string.information_dialog_title))
                // getText returns the string itself so there's no need for html parsing as opposed to getresources.getstring
                .setMessage(getText(R.string.information_dialog_msg))
                .setPositiveButton(R.string.information_dialog_btn_msg, (dialog, which) ->
                        dialog.dismiss()
                );

        setCancelable(false);
        return alertDialogBuilder.create();
    }

    //Set links clickable (this gets called after onCreateDialog)
    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        ((TextView) d.findViewById(android.R.id.message)).setMovementMethod(LinkMovementMethod.getInstance());

    }
}
