package nl.d_tt.rsr.ui.fragment.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import nl.d_tt.rsr.R;

public class ExitDialogFragment extends DialogFragment {

    //Empty constructor, will call onCreateDialog automatically
    public ExitDialogFragment() {

    }

    public static ExitDialogFragment newInstance() {
        return new ExitDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.AlertDialogTheme);

        alertDialogBuilder.setMessage(R.string.exit_dialog_msg)
                .setNegativeButton(getResources().getString(R.string.exit_dialog_negative_btn_msg), (dialog, which) ->
                        dialog.dismiss()
                )
                /*
                   Positive button - "OK". Since we will never a situation where there will be more than this activity
                   on the stack, we can safely use exit(0). Using finish() would not have the expected result since the postDelayed handle
                   will still start the menu activity. I assume the model applicaiton does use a variation of finish(), because if we click the button
                   in about the last second of the 4 second timeframe, the following activity still starts
                   (although logically speaking, it is not supposed to, so I would guess this is a bug)
                 */
                .setPositiveButton(getResources().getString(R.string.exit_dialog_positive_btn_msg), (dialog, which) ->

                        System.exit(0)
                );

        return alertDialogBuilder.create();
    }
}
