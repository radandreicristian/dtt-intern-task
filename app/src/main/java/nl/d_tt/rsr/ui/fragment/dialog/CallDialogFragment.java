package nl.d_tt.rsr.ui.fragment.dialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.os.Bundle;
import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.View;

import java.util.Objects;

import nl.d_tt.rsr.R;

public class CallDialogFragment extends DialogFragment {

    //Empty constructor, will call onCreateDialog automatically
    public CallDialogFragment() {

    }

    public static CallDialogFragment newInstance() {
        return new CallDialogFragment();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity(), R.style.CallDialogStyle);

        //Set the view of the dialog to the dialog_call layout
        @SuppressLint("InflateParams") View dialogView = Objects.requireNonNull(getActivity()).getLayoutInflater().inflate(R.layout.dialog_call, null);
        alertDialogBuilder.setView(dialogView);
        setCancelable(false);

        return alertDialogBuilder.create();
    }
}
