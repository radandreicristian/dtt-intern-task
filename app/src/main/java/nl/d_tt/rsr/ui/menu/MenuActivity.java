package nl.d_tt.rsr.ui.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ImageButton;

import nl.d_tt.rsr.R;
import nl.d_tt.rsr.ui.fragment.dialog.InformationDialogFragment;
import nl.d_tt.rsr.ui.map.MapActivity;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Toolbar toolbar = findViewById(R.id.activity_menu_toolbar);
        toolbar.setTitle(R.string.main_toolbar_title);

        boolean isTablet = getResources().getBoolean(R.bool.isTablet);
        if (isTablet) {
            Button infoButton = findViewById(R.id.activity_menu_info_button);
            infoButton.setOnClickListener(v ->
                    displayInformationDialog()
            );
        } else {
            ImageButton infoButton = toolbar.findViewById(R.id.activity_menu_info_button);
            infoButton.setOnClickListener(v ->
                    displayInformationDialog()
            );
        }

        Button bottomButton = findViewById(R.id.activity_menu_bottom_button);
        bottomButton.setOnClickListener(v ->
                goToMapActivity()
        );
    }

    public void displayInformationDialog() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        InformationDialogFragment informationDialogFragment = InformationDialogFragment.newInstance();
        informationDialogFragment.show(fragmentManager, "");
    }

    public void goToMapActivity() {
        Intent intent = new Intent(MenuActivity.this, MapActivity.class);
        startActivity(intent);
    }

}