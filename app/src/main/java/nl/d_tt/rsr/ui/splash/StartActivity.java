package nl.d_tt.rsr.ui.splash;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Intent;
import android.os.Handler;

import nl.d_tt.rsr.R;
import nl.d_tt.rsr.ui.fragment.dialog.ExitDialogFragment;
import nl.d_tt.rsr.ui.menu.MenuActivity;

public class StartActivity extends AppCompatActivity {

    /*
        4 seconds after the activity starts, it opens the menu activity
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        new Handler().postDelayed(this::startMenuActivity, 4000);
    }

    /*
        If  we press "back" during the splash screen, we get a dialog to close the application
     */
    @Override
    public void onBackPressed(){
        showAlertDialog();
    }

    private void showAlertDialog(){
        FragmentManager fragmentManager = getSupportFragmentManager();
        ExitDialogFragment exitDialogFragment = ExitDialogFragment.newInstance();
        exitDialogFragment.show(fragmentManager, "");
    }
    /*
        Flags added to the intent to remove the splash screen from the back stack
     */
    private void startMenuActivity(){
        Intent intent = new Intent(StartActivity.this, MenuActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
