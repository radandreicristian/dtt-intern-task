package nl.d_tt.rsr.ui.map;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import nl.d_tt.rsr.R;

class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

    private final Context mContext;

    public CustomInfoWindowAdapter(Context context) {
        mContext = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {

        @SuppressLint("InflateParams") View view = ((Activity) mContext).getLayoutInflater().inflate(R.layout.layout_info_window, null);
        TextView titleTextView = view.findViewById(R.id.info_window_title);
        TextView addressTextView = view.findViewById(R.id.info_window_address);
        TextView bottomTextView = view.findViewById(R.id.info_window_bottom_text);

        titleTextView.setText(mContext.getResources().getString(R.string.info_window_title));
        addressTextView.setText(marker.getSnippet());
        bottomTextView.setText(mContext.getResources().getString(R.string.info_window_msg));

        return view;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
